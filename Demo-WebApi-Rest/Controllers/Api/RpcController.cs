﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DemoWebApiRest.Data;
using DemoWebApiRest.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DemoWebApiRest.Controllers.Api
{
    [Route("/rpc")]
    [Produces("application/json")]
    public class RpcController : ControllerBase
    {
        private readonly ApplicationDbContext _ctx;
        
        public RpcController(ApplicationDbContext dbContext)
        {
            _ctx = dbContext;
        }

        [HttpGet("get-all-books")]
        public IEnumerable<Book> ReadAllBooks()
        {
            return _ctx.Books.AsEnumerable();
        }
        
        [HttpGet("get-book")]
        public Book ReadBook(int bookId)
        {
            return _ctx.Books.Find(bookId);
        }
        
        [HttpPost("create-book")]
        public Book CreateBook([FromBody]Book newBook)
        {
            _ctx.Books.Add(newBook);
            _ctx.SaveChanges();
            
            return newBook;
        }
        
        [HttpPost("update-book")]
        public void ChangeBook([FromBody]Book changedBook)
        {
            _ctx.Books.Update(changedBook);
            _ctx.SaveChanges();
        }
        
        [HttpPost("delete-book")]
        public void DeleteBook(int bookId)
        {
            var bookToRemove = _ctx.Books.Find(bookId);
            _ctx.Books.Remove(bookToRemove);
            _ctx.SaveChanges();
        }
    }
}