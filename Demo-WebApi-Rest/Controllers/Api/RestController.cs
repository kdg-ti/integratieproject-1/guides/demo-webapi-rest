﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using DemoWebApiRest.Data;
using DemoWebApiRest.Models;

namespace DemoWebApiRest.Controllers.Api
{
    [ApiController]
    [Route("/rest/books")]
    public class RestController : ControllerBase
    {
        private readonly ApplicationDbContext _ctx;
        
        public RestController(ApplicationDbContext dbContext)
        {
            _ctx = dbContext;
        }

        [HttpGet]
        public IActionResult ReadAllBooks()
        {
            var books = _ctx.Books.AsEnumerable();

            //if (books == null || !books.Any())
            //    return NoContent();
            
            return Ok(books);
        }
        
        [HttpGet("{bookId}")]
        public IActionResult ReadBook(int bookId)
        {
            var book = _ctx.Books.Find(bookId);

            if (book == null)
                return NotFound();
            
            return Ok(book);
        }
        
        [HttpPost]
        public IActionResult CreateBook(/*[FromBody]*/Book newBook)
        {
            /*if (!ModelState.IsValid)
                return BadRequest(ModelState);*/
            
            _ctx.Books.Add(newBook);
            _ctx.SaveChanges();
            
            //return CreatedAtAction("ReadBook", new { id = newBook.BookId }, newBook);
            return CreatedAtAction(nameof(ReadBook), new { bookId = newBook.BookId }, newBook);
        }
        
        [HttpPut("{bookId}")]
        public IActionResult ChangeBook(int bookId, /*[FromBody]*/Book changedBook)
        {
            var book = _ctx.Books.Find(bookId);

            if (book == null)
                return NotFound();

            /*if (!ModelState.IsValid)
                return BadRequest(ModelState);*/

            _ctx.Books.Update(changedBook);
            _ctx.SaveChanges();

            return NoContent();
        }
        
        [HttpDelete("{bookId}")]
        public IActionResult DeleteBook(int bookId)
        {
            var bookToRemove = _ctx.Books.Find(bookId);
            
            if (bookToRemove == null)
                return NotFound();

            _ctx.Books.Remove(bookToRemove);
            _ctx.SaveChanges();

            return NoContent();
        }
    }
}